django\_wireguard.management.commands package
=============================================

Submodules
----------

django\_wireguard.management.commands.clear\_private\_keys module
-----------------------------------------------------------------

.. automodule:: django_wireguard.management.commands.clear_private_keys
   :members:
   :undoc-members:
   :show-inheritance:

django\_wireguard.management.commands.create\_peer module
---------------------------------------------------------

.. automodule:: django_wireguard.management.commands.create_peer
   :members:
   :undoc-members:
   :show-inheritance:

django\_wireguard.management.commands.delete\_peers module
----------------------------------------------------------

.. automodule:: django_wireguard.management.commands.delete_peers
   :members:
   :undoc-members:
   :show-inheritance:

django\_wireguard.management.commands.setup\_interface module
-------------------------------------------------------------

.. automodule:: django_wireguard.management.commands.setup_interface
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: django_wireguard.management.commands
   :members:
   :undoc-members:
   :show-inheritance:
