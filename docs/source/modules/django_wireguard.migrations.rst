django\_wireguard.migrations package
====================================

Submodules
----------

django\_wireguard.migrations.0001\_initial module
-------------------------------------------------

.. automodule:: django_wireguard.migrations.0001_initial
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: django_wireguard.migrations
   :members:
   :undoc-members:
   :show-inheritance:
