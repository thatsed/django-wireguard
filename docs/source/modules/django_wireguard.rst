django\_wireguard package
=========================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   django_wireguard.management
   django_wireguard.migrations
   django_wireguard.templatetags
   django_wireguard.tests
   django_wireguard.wagtail

Submodules
----------

django\_wireguard.admin module
------------------------------

.. automodule:: django_wireguard.admin
   :members:
   :undoc-members:
   :show-inheritance:

django\_wireguard.apps module
-----------------------------

.. automodule:: django_wireguard.apps
   :members:
   :undoc-members:
   :show-inheritance:

django\_wireguard.forms module
------------------------------

.. automodule:: django_wireguard.forms
   :members:
   :undoc-members:
   :show-inheritance:

django\_wireguard.models module
-------------------------------

.. automodule:: django_wireguard.models
   :members:
   :undoc-members:
   :show-inheritance:

django\_wireguard.settings module
---------------------------------

.. automodule:: django_wireguard.settings
   :members:
   :undoc-members:
   :show-inheritance:

django\_wireguard.signals module
--------------------------------

.. automodule:: django_wireguard.signals
   :members:
   :undoc-members:
   :show-inheritance:

django\_wireguard.sync\_wg module
---------------------------------

.. automodule:: django_wireguard.sync_wg
   :members:
   :undoc-members:
   :show-inheritance:

django\_wireguard.utils module
------------------------------

.. automodule:: django_wireguard.utils
   :members:
   :undoc-members:
   :show-inheritance:

django\_wireguard.validators module
-----------------------------------

.. automodule:: django_wireguard.validators
   :members:
   :undoc-members:
   :show-inheritance:

django\_wireguard.wireguard module
----------------------------------

.. automodule:: django_wireguard.wireguard
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: django_wireguard
   :members:
   :undoc-members:
   :show-inheritance:
